
<!DOCTYPE html>
<html>
<head>

  <script src="js/vendor/jquery.js"></script>
  <script src="js/vendor/underscore.js"></script>
  <script src="js/vendor/backbone.js"></script>
  <script src="js/vendor/backbone.marionette.js"></script>
  <script src="js/action-prop.js"></script>

<script type="application/javascript" > </script>

</head>
<body>
<h1>action property</h1>
<div id="content"> </div>

<script type="text/template" id="example-template">
 <button action='click:accept'>click me please</button>
 <p action='accept'>with out event </p>
 <input action='change:inputChanged' value="change me please" />
 <a href="#">link to nowhere</a>
</script>


</body>
</html>
