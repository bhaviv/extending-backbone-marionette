//*********** working example
//$(function(){
//
//  var myView =  Backbone.Marionette.ItemView.extend({
//    initialize: function(){
//      this.on('render',this.registerAutoEvents);
//    },
//    el: $('#content'),
//    template: '#example-template',
//
//    registerAutoEvents: function(){
//     var viewObj = this;
//     this.$el.find('[action]').each(function(){
//       $this = $(this);
//       var action= $this.attr('action');
//       action = action.split(':');
//       if(action.length < 2) return;
//       $this.bind(action[0],viewObj[action[1]]);
//     });
//    },
//
//    accept: function(e){
//      alert('button clicked me');
//    },
//    inputClick: function(e){
//      alert('input clicked ');
//    }
//
//  });
//
//  var mv = new myView();
//  mv.render();
//});


//####################################

String.prototype.capitalize = function () {
  return this.charAt(0).toUpperCase() + this.slice(1);
}

var MyApp = new Marionette.Application();

MyApp.module("Common.Mixing", function (Mixing, MyApp, Backbone, Marionette, $, _) {

  Mixing.addAction = function (viewObj) {
    viewObj.registerAutoEvents = function () {
      viewObj.$el.find('[action]').each(function () {
        $this = $(this);
        var action = $this.attr('action');
        action = action.split(':');
        var event, handler;
        if (action.length == 1) {
          event = 'click';
          handler = 'on' + action[0].capitalize();
        } else {
          event = action[0];
          handler = 'on' + action[1].capitalize();
        }
        $this.on(event, viewObj[handler]);
        console.log(action);
      });
    };

    viewObj.on('render', viewObj.registerAutoEvents);

    _.bind(viewObj.registerAutoEvents, viewObj);

  };
});
// end module


$(function () {
  var MyView= Backbone.Marionette.CompositeView.extend({
    el: $('#content'),
    template: '#example-template',
    events: {
      'click a': 'linkClicked'
    },
    initialize: function () {
      MyApp.Common.Mixing.addAction(this);
    },
    onAccept: function (e) {
      alert('something clicked ');
    },
    onInputChanged: function (e) {
      alert('input Changed');
    },
    linkClicked: function () {
      alert('linked clicked');
    }
  });


  mv = new MyView();
  mv.render();
});
