//Extending backbone functions
(function(){
 // extending backbone set function
 var oldSet = Backbone.Model.prototype.set ;
 Backbone.Model.prototype.set =  function(key, value, options) {
     var attrs, attr;
     // Normalize the key-value into an object
     if (_.isObject(key) || key === null) {
       attrs = key;
       options = value;
     } else {
       attrs = {};
       attrs[key] = value;
     }
     // always pass an options hash around. This allows modifying
     // the options inside the setter
     options = options || {};
     // Go over all the set attributes and call the setter if available
     for (attr in attrs) {
       if (_.isFunction(this.setters[attr])) {
         attrs[attr] = this.setters[attr].call(this, attrs[attr], options);
       }
     }
     return oldSet.call(this,attrs,options);
   };

 Backbone.Model.prototype.setters = {};
})();


//checking the code
 var myModel = Backbone.Model.extend({
   setters:{
     first: function(first){
       return first + 10;
     }
   }
 });
 var mm = new myModel();


console.group();
 mm.set('first',1);
 console.log('first:' + mm.get('first'));

 mm.set('second',2);
 console.log('second:' + mm.get('second'));
console.groupEnd();

mm.set({first:5,second:6});

console.group();
console.log('first:' + mm.get('first'));
console.log('second:' + mm.get('second'));
console.groupEnd();

$(function(){
 var myView =  Backbone.View.extend({
    model:mm,
    el: $('#content'),
    render: function(){
     var template = _.template($('#example-template').html(),{first:mm.get('first'),second: mm.get('second')});
     this.$el.html(template);
    }
  });

 var mv = new myView();
 mv.render();
});
