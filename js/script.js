var MyApp= new Marionette.Application();

var itemView = Marionette.ItemView.extend({});

//Marionette module
MyApp.module("Common.Views", function(Views, ContactManager, Backbone, Marionette, $, _){
});


//model
Entities.Contact = Backbone.Model.extend({});

Entities.ContactCollection = Backbone.Collection.extend({});


ContactManager.reqres.setHandler("contact:entities", function(){
  return API.getContactEntities();
});
